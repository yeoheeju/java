package MyClass;

public class McVO {

	//필드
	private int code;
	private int memNo;
	private String date;

	//메소드
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getMemNo() {
		return memNo;
	}

	public void setMemNo(int memNo) {
		this.memNo = memNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	@Override
	public String toString() {
		return "[수강코드 = " + code + ", 회원번호 = " + memNo + ", 수강신청 날짜 = " + date + "]";
	}

}
