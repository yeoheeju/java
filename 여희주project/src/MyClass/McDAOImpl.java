package MyClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.yedam.common.DAO;

public class McDAOImpl extends DAO implements McDAO {

	// 싱글톤
	private static McDAO instance = null;

	public static McDAO getInstance() {
		if (instance == null)
			instance = new McDAOImpl();
		return instance;
	}

	@Override
	public void insert(McVO mcVO) {
		try {
			String pattern = "yyyy-MM-dd"; //수강신청 한 날짜
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			//SimpleDateFormat으로 오늘 날짜 출력
			
			String date = simpleDateFormat.format(new Date());
			
			connect();
			String sql = "INSERT INTO MyClass VALUES (?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, mcVO.getCode());
			pstmt.setInt(2, mcVO.getMemNo());
			pstmt.setString(3, (date));
			int result = pstmt.executeUpdate();

			if (result > 0) {
				System.out.println("정상적으로 신청되었습니다.");
			} else {
				System.out.println("정상적으로 신청되지 않았습니다.");
			}

		} catch (Exception e) { //모든 예외의 부모격 /error에 일종으로 Exception 예외가 발생할 것을 대비하여 미리 이를 소스상에서 제어하고 처리하도록 만드는 것
			System.out.println("이미 신청된 강의입니다.");  //신청된 강의 중복 안되게 막음
		} finally {
			disconnect();
		}

	}

	@Override
	public void delete(int code) { //코드로  수강신청 삭제
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM MyClass WHERE code = " + code;

			int result = stmt.executeUpdate(sql);
			if (result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			} else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace(); //예외처리를 정상적으로 하고 나서 예외 메시지를 추가로 띄울 수 있다 /어디서 오류났는지 찾을수 있음
		} finally {
			disconnect();
		}

	}

	@Override
	public List<McVO> selectAll2() {
		List<McVO> list = new ArrayList<>();
		try {
			connect();

			stmt = conn.createStatement();
			String sql = "SELECT * FROM MyClass";
			rs = stmt.executeQuery(sql);

			int count = 0;
			while (rs.next()) {
				McVO mcVO = new McVO();
				mcVO.setCode(rs.getInt("code"));
				mcVO.setMemNo(rs.getInt("mem_no"));
				mcVO.setDate(rs.getString("date"));

				list.add(mcVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return list;
	}

}
