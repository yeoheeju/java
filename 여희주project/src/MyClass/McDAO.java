package MyClass;

import java.util.List;

public interface McDAO {
	
	//전체조회
	List<McVO> selectAll2();

	// 등록
	void insert(McVO mcVO);

	// 삭제
	void delete(int code);
}


