package com.yedam.course;

import java.util.List;

public interface CourseDAO {

	// 전체조회(해당 테이블이 가지고있는 모든정보 가져옴)
	List<CourseVO> selectAll();

	// 단건조회
	CourseVO selectOne(CourseVO courseVO);

}
