package com.yedam.course;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class CourseDAOImpl extends DAO implements CourseDAO {

	// 싱글톤 - 1:1 / 하나의 객체만 만들도록 할때 사용 /외부에서 new연산자로 호출 못하도록 private / 필드에 자기자신의 객체 static 선언
		private static CourseDAO instance = null;

		public static CourseDAO getInstance() { //객체를 반환
			if (instance == null)
				instance = new CourseDAOImpl();
			return instance;
		}
		
	
	@Override
	public List<CourseVO> selectAll() {
		List<CourseVO> list = new ArrayList<>();
		try { //try-catch-(finally) : 오류 난 경우 그에대한 예외처리하기 위해 사용 / finally가 붙으면 try 블록과 상관없이 finally구문 실행
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM Course"; // select * = 모든 속성 값을 조회 / from 테이블에서
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) { //전체조회
				CourseVO courseVO = new CourseVO();
				courseVO.setCode(rs.getInt("code"));
				courseVO.setCourse(rs.getString("course"));
				courseVO.setLectureroom(rs.getString("lecture_room"));
				courseVO.setTime(rs.getString("time"));
				courseVO.setPeriod(rs.getString("period"));
				courseVO.setPname(rs.getString("pname"));
				
				list.add(courseVO);
			}
		} catch(Exception e) { //모든 예외의 부모격 /error에 일종으로 Exception 예외가 발생할 것을 대비하여 미리 이를 소스상에서 제어하고 처리하도록 만드는 것
			e.printStackTrace(); //예외처리를 정상적으로 하고 나서 예외 메시지를 추가로 띄울 수 있다
		} finally {
			disconnect();
		}
		return list;
	}

	@Override
	public CourseVO selectOne(CourseVO courseVO) { // 조건걸고 전체조회
		CourseVO findVO = null;
		try {
			connect();
			stmt = conn.createStatement(); //Statement를 사용하여 sql문에 필요한 데이터를 입력 받고 실행시 발생한 데이터를 ResultSet 에 저장한다
			
			String sql = "SELECT code, lectureroom, pname FROM Course WHERE code = " + courseVO.getCode(); // select * - 모든 속성 값을 조회 / from 테이블 / where - 특정한 조건에 한해서 결과를 보는 기능
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				findVO = new CourseVO();
				findVO.setCode(rs.getInt("code"));
				findVO.setCourse(rs.getString("course"));
				findVO.setLectureroom(rs.getNString("lectureroom"));
				findVO.setTime(rs.getString("time"));
				findVO.setPeriod(rs.getString("period"));
				findVO.setPname(rs.getString("pname"));

				//대소문자 구분하기 / 대문자 - 객체 / 소문자 - 필드 등 나머지
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return findVO; 
	}
}
