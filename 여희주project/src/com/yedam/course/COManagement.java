package com.yedam.course;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.yedam.login.LoginControl;
import com.yedam.login.LoginManagement;
import com.yedam.member.MbVO;

import MyClass.McDAO;
import MyClass.McDAOImpl;
import MyClass.McVO;

public class COManagement extends LoginManagement {

	Scanner sc = new Scanner(System.in); // 정수, 실수, 문자열을 읽어올수있음 / import를 통해 외부클래스 호출
	CourseDAO courseDAO = CourseDAOImpl.getInstance(); //하나의 인스턴스만 가지고 공유
	McDAO mcDAO = McDAOImpl.getInstance();  //management 따로 만들 필요없이 MyClass 선언해서 연결 
	
	public COManagement() { //이름 확인 제대로(걍 내가 자주틀림)
			while (true) {
				// 메뉴출력
				menuPrint();

				// 메뉴선택
				int menuNo = menuSelect();

				// 각 메뉴의 기능을 실행
				if (menuNo == 1) {
					// 전체조회
					selectAll();
				} else if (menuNo == 2) {
					// 수강신청
					insertMyClass();
				} else if (menuNo == 3) {
					//수강확인
					selectAll2();
				} else if (menuNo == 4) {
					// 수강취소
					deleteMyClass();
				} else if (menuNo == 9) {
					// 종료
					end();
					break;
				} else {
					// 기타사항
					printErrorMessage();
				}
			}
		}


	private void printErrorMessage() {
		System.out.println("===============================================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 한번 확인해주세요.");
		System.out.println("===============================================");
	}

	private void end() {
		System.out.println("===============================================");
		System.out.println("프로그램을 종료합니다.");
		System.out.println("===============================================");
	}

	protected void menuPrint() {
		System.out.println("===============================================");
		System.out.println("1.전체조회 2.수강신청 3.수강확인 4.수강취소 9.종료");
		System.out.println("===============================================");
	}

	protected int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) { //숫자 예외
			System.out.println("메뉴는 숫자로 구성되어 있습니다.");
		}
		return menuNo;
	}

	private void selectAll() { //전제조회
		List<CourseVO> list = courseDAO.selectAll(); //list<> list = / 데이터 용도가 바뀌는 상황이 생겨도 선언부만 수정하면 사용가능 
		if (list.isEmpty()) {  //문자열의 길이를 체크, 문자열의 길이가 0인 경우에만 true를 리턴하기 때문에 빈공백이 들어있는 문자열은 false를 리턴
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}

		for(CourseVO courseVO : list) {
			System.out.printf("%d : %s, %s, %s, %s, %s \n", courseVO.getCode(), courseVO.getCourse(), courseVO.getLectureroom(), courseVO.getTime(), courseVO.getPeriod(), courseVO.getPname());
			//갯수 맞는지 확인하기(갯수 확인 안해서 계속 오류떴었음)
		}
	}
	
	private void selectAll2() { //수강신청 후 목록확인
		List<McVO> list = mcDAO.selectAll2();
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		
		for(McVO mcVO : list) { //목록 확인할때 코드, 회원번호, 신청날짜 출력
			System.out.printf("수강코드 : %d, 회원번호 : %d, 신청날짜 : %s \n", mcVO.getCode(), mcVO.getMemNo(), mcVO.getDate());
		}
		
	}

	private void insertMyClass() { //수강신청
		//insertCourse 대신 MyClass 값을 넣어줘야함
		
		McVO mcVO = inputmyclassAll();//수강번호 입력
		MbVO loginInfo = LoginControl.getLoginInfo(); //LoginControl에서 로그인 정보 가져옴
		mcVO.setMemNo(loginInfo.getMemNo());
		//loginInfo를 mcVO랑 연결해서 담기
		
		mcDAO.insert(mcVO);
	}
	

	private void deleteMyClass() { //수강삭제
		int code = inputCode();
		mcDAO.delete(code);
	}
	
	private McVO inputmyclassAll() { 
		McVO mcVO = new McVO();
		
		System.out.println("수강번호 > ");
		mcVO.setCode(Integer.parseInt(sc.nextLine()));
				
		return mcVO;
	}

	private CourseVO inputCourseInfo() {
		CourseVO courseVO = new CourseVO();

		System.out.println("수강번호 > ");
		courseVO.setCode(Integer.parseInt(sc.nextLine()));

		return courseVO;
	}

	private int inputCode() {
		int code = 0;
		try {
			System.out.println("수강코드를 입력하세요.");
			code = (Integer.parseInt(sc.nextLine()));
		} catch (NumberFormatException e) {
			System.out.println("수강번호는 숫자로 구성되어 있습니다.");
		}
		return code;
	}
}
