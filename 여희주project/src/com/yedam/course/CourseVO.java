package com.yedam.course;

public class CourseVO {

	//필드
	private int code;
	private String course;
	private String lectureroom;
	private String time;
	private String period;
	private String pname;
	
	//메소드
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getLectureroom() {
		return lectureroom;
	}
	public void setLectureroom(String lectureroom) {
		this.lectureroom = lectureroom;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	
	
	@Override
	public String toString() {
		return "CourseVO [code=" + code + ", course=" + course + ", lectureroom=" + lectureroom + ", time=" + time
				+ ", period=" + period + ", pname=" + pname + "]";
	}

	

}
