package com.yedam.member;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class MbDAOImpl extends DAO implements MbDAO {
	
    //테이블에 접근해서 조회, 수정,삭제
	// 싱글톤 - 1:1 / 하나의 객체만 만들도록 할때 사용 /외부에서 new연산자로 호출 못하도록 private / 필드에 자기자신의 객체 static 선언
	private static MbDAO instance = null;
	public static MbDAO getInstance() { //객체를 반환
		if (instance == null)
			instance = new MbDAOImpl();
		return instance;
	}

	@Override //자식클래스에서 동일한 메소드 재정의
	public List<MbVO> selectAll() { 
		List<MbVO> list = new ArrayList<>(); //list<> list = / 데이터 용도가 바뀌는 상황이 생겨도 선언부만 수정하면 사용가능 
		try { //try-catch-(finally) : 오류 난 경우 그에대한 예외처리하기 위해 사용 / finally가 붙으면 try 블록과 상관없이 finally구문 실행
			connect();

			stmt = conn.createStatement(); //Statement를 사용하여 sql문에 필요한 데이터를 입력 받고 실행시 발생한 데이터를 ResultSet 에 저장
			String sql = "SELECT * FROM Member"; // select * = 모든 속성 값을 조회 / from 테이블에서
			rs = stmt.executeQuery(sql); //ResultSet 객체에 결과값을 담을 수 있음, 객체 값 반환
			
			while (rs.next()) {
				MbVO mbVO = new MbVO();
				mbVO.setMemNo(rs.getInt("mem_no"));
				mbVO.setPw(rs.getString("pw"));
				mbVO.setName(rs.getString("name"));
				mbVO.setGender(rs.getString("gender"));
				mbVO.setBirthdate(rs.getString("birthday_date"));
				mbVO.setAddress(rs.getString("address"));
				mbVO.setTel(rs.getString("tel"));

				list.add(mbVO);
			}
		} catch (Exception e) { //모든 예외의 부모격 /error에 일종으로 Exception 예외가 발생할 것을 대비하여 미리 이를 소스상에서 제어하고 처리하도록 만드는 것
			e.printStackTrace();
		} finally {
			disconnect(); //연결되어있는것을 끊을때 // disconnect 사용시 connect 사용하면 다시 복구가능
		}
		return list;
	}

	@Override
	public List<MbVO> selectOne(MbVO mbVO) {// 리스트는 순서가 있는 데이터들의 집합 / 불연속적인 메모리 공간에 데이터들이 연관되며 포인터를 통해 각 데이터들이 연결되고
		                                                                //동적으로 크기가 정해지며 데이터의 삽입, 삭제가 배열에 비해 용이하고 메모리의 재사용성이 높아짐
		List<MbVO> list = new ArrayList<>();
		MbVO findVO = null;
		
		try {
			connect();
			stmt = conn.createStatement(); //Statement를 사용하여 sql문에 필요한 데이터를 입력 받고 실행시 발생한 데이터를 ResultSet 에 저장한다

			String sql = "SELECT * FROM Member WHERE name = '" + mbVO.getName()+ "'"; // select * - 모든 속성 값을 조회 / from 테이블이름 / where - 특정한 조건에 한해서 결과를 보는 기능
			//1.Member 테이블에 담긴 정보중에 이름으로 검색
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				findVO = new MbVO();
				findVO.setMemNo(rs.getInt("mem_no"));
				findVO.setName(rs.getString("name"));
				findVO.setTel(rs.getString("tel"));
				//2. 회원번호, 이름, 전화번호 출력
				list.add(findVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return list;
	}
	
	public MbVO loginInfo(MbVO mbVO) {
		MbVO loginInfo = null;
		try {
			connect();
			String sql = "SELECT * FROM member WHERE mem_no = '" + mbVO.getMemNo() + "'";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				// 아이디 존재
				if (rs.getString("pw").equals(mbVO.getPw())) {
					// 비밀번호 일치
					// -> 로그인 성공
					loginInfo = new MbVO();
					loginInfo.setMemNo(rs.getInt("mem_no"));
					loginInfo.setPw(rs.getString("pw"));
					loginInfo.setAdmim(rs.getString("admin"));
				} else {
					System.out.println("비밀번호가 일치하지 않습니다.");
				}
			} else {
				System.out.println("회원번호가 존재하지 않습니다.");
			}

		} catch (Exception e) { //모든 예외의 부모격 /error에 일종으로 Exception 예외가 발생할 것을 대비하여 미리 이를 소스상에서 제어하고 처리하도록 만드는 것
			e.printStackTrace(); //예외처리를 정상적으로 하고 나서 예외 메시지를 추가로 띄울 수 있다
		} finally {
			disconnect();
		}
		return loginInfo;
	}

	@Override
	public void insert(MbVO mbVO) { //insert로 해당정보들 입력
		try {
			connect();
			String sql = "INSERT INTO Member VALUES (?, ?, ?, ?, ?, ?, ?, ?)"; //생성한 테이블에 데이터 저장하기 / 물음표 갯수 확인
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, mbVO.getMemNo());
			pstmt.setString(2, mbVO.getPw());
			pstmt.setString(3, mbVO.getName());
			pstmt.setString(4, mbVO.getGender());
			pstmt.setString(5, mbVO.getBirthdate());
			pstmt.setString(6, mbVO.getAddress());
			pstmt.setString(7, mbVO.getTel());
			pstmt.setString(8, mbVO.getAdmim());

			int result = pstmt.executeUpdate();

			if (result > 0) {// true
				System.out.println("정상적으로 등록되었습니다.");
			} else { //false
				System.out.println("정상적으로 등록되지 않았습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}

	@Override
	public void update(MbVO mbVO) { // update(수정)으로 이름, 주소, 번호변경
		try {
			connect();
			String sql = "UPDATE Member SET address = ?, tel = ? WHERE name = ?"; // UPDATE(테이블이름) / SET(필드 이름)=(수정할 데이터) / WHERE;
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, mbVO.getAddress());
			pstmt.setString(2, mbVO.getTel());
			pstmt.setString(3, mbVO.getName());

			int result = pstmt.executeUpdate();

			if (result > 0) { // true
				System.out.println("정상적으로 수정되었습니다.");
			} else { //false
				System.out.println("정상적으로 수정되지 않았습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}

	@Override
	public void delete(int mbVO) {
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM Member WHERE mem_no = " + mbVO; //Member 테이블의 회원번호로 삭제

			int result = stmt.executeUpdate(sql);
			if (result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			} else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

}
