package com.yedam.member;

import java.util.List;

public interface MbDAO { //코드와 객체가 서로 통신하는 접점역할 

	// 전체조회(해당 테이블이 가지고있는 모든정보 가져옴)
	List<MbVO> selectAll();

	// 전체조회(조건걸고 조회)
	List<MbVO> selectOne(MbVO mbVO);

	// 등록
	void insert(MbVO mbVO);

	// 수정
	void update(MbVO mbVO);

	// 삭제
	void delete(int mbVO);

	//로그인
	MbVO loginInfo(MbVO inputInfo);
}
