package com.yedam.member;

public class MbVO {

	//필드
	private int memNo;
	private String pw;
	private String name;
	private String gender;
	private String birthdate;
	private String address;
	private String tel;
	private String admim;
	
	
	//메소드
	public int getMemNo() {
		return memNo;
	}
	public void setMemNo(int memNo) {
		this.memNo = memNo;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAdmim() {
		return admim;
	}
	public void setAdmim(String admim) {
		this.admim = admim;
	}
	
	
	@Override
	public String toString() { //출력할 정보를 다 적어놔도 여기서 원하는것만 선택해서 출력가능
		return "[회원번호 = " + memNo + ", 이름 = " + name + ", 휴대폰번호 = " + tel + "]";
	}
	

	

}
