package com.yedam.member;

import java.util.List;
import java.util.Scanner;

import com.yedam.login.LoginManagement;

public class MbManagement extends LoginManagement {


	Scanner sc = new Scanner(System.in); // 정수, 실수, 문자열을 읽어올수있음 / import를 통해 외부클래스 호출
	MbDAO mbDAO = MbDAOImpl.getInstance(); //하나의 인스턴스만 가지고 공유

	public MbManagement() {
		while (true) {
			boolean role = selectRole(); 
			
			// 메뉴출력
			menuPrint(role);

			// 메뉴선택
			int menuNo = menuSelect();

			// 각 메뉴의 기능 실행
			if (menuNo == 1 && role) {
				// 회원등록
				insertMb();
			} else if (menuNo == 2 && role) {
				// 회원삭제
				deleteMb();
			} else if (menuNo == 3 && role) {
				// 전체조회
				selectAll();
			} else if (menuNo == 4 && role) {
				// 단건조회
				selectOne();
			} else if (menuNo == 5 && role) {
				// 정보수정
				updateMb();
			} else if (menuNo == 9) {
				// 종료
				end();
				break;
			} else {
				// 기타사항
				printErrorMessage();
			}
		}
	}

	private void printErrorMessage() { //오류
		System.out.println("===============================================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 한번 확인해주세요.");
		System.out.println("===============================================");
	}

	private void end() {
		System.out.println("===============================================");
		System.out.println("프로그램을 종료합니다.");
		System.out.println("===============================================");
	}

	protected void menuPrint(boolean role) {  // boolean - 가져올 객체가 있으면 true 없으면 false
		String menu = "";
		if(role) { //관리자 권한
			menu += "1.회원등록 "
				  + "2.회원삭제 "
				  + "3.전체조회 "
				  + "4.단건조회 "
				  + "5.정보수정 ";
		}
		menu += "9.종료 "; //회원
		
		System.out.println("===============================================");
		System.out.println(menu);
		System.out.println("===============================================");
	}
	

	protected int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어 있습니다.");
		}
		return menuNo;
	}

	private void selectAll() {
		List<MbVO> list = mbDAO.selectAll();
		if (list.isEmpty()) { //문자열의 길이를 체크, 문자열의 길이가 0인 경우에만 true를 리턴하기 때문에 빈공백이 들어있는 문자열은 false를 리턴
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}

		for (MbVO mbVO : list) {
			System.out.printf("회원번호 : %d, 이름 : %s, 생년월일 : %s, 주소 : %s, 전화번호 : %s \n", mbVO.getMemNo(), mbVO.getName(), mbVO.getBirthdate(), mbVO.getAddress(), mbVO.getTel());
		}
	}

	private void selectOne() { //이름으로 중복확인
		MbVO findMb = inputMbInfo();
		List<MbVO> list = mbDAO.selectOne(findMb);
		
		if(list.isEmpty()) {
			System.out.printf("%s 회원은 존재하지 않습니다.\n", findMb.getName()); //1.이름으로 조회
			return;
		} 
		System.out.println("검색결과 > ");
		
		for(MbVO mbVO : list) { // 향상된 for문 사용
			System.out.printf("회원번호 : %d, 이름 : %s, 전화번호 : %s \n", mbVO.getMemNo(), mbVO.getName(), mbVO.getTel()); //2.출력할때는 3가지 나오게
		}	
	}

	private void insertMb() {
		MbVO mbVO = inputMbAll();
		mbDAO.insert(mbVO);
	}

	private void updateMb() {
		MbVO mbVO = inputMb();
		mbDAO.update(mbVO);
	}

	private void deleteMb() {
		int memNo = inputMemNo();
		mbDAO.delete(memNo);
	}
	
	private MbVO inputMb() {
		//수정
		MbVO mbVO = new MbVO();
		System.out.println("이름 > ");
		mbVO.setName(sc.nextLine());
		
		System.out.println("주소 > ");
		mbVO.setAddress(sc.nextLine());
		
		System.out.println("연락처 > ");
		mbVO.setTel(sc.nextLine());
		
		return mbVO;
	}

	private MbVO inputMbAll() { // 데이터를 입력받을때
		MbVO mbVO = new MbVO();
		System.out.println("회원번호 > ");
		mbVO.setMemNo(Integer.parseInt(sc.nextLine()));
		
		System.out.println("비밀번호 > ");
		mbVO.setPw(sc.nextLine());
		
		System.out.println("이름 > ");
		mbVO.setName(sc.nextLine());
		
		System.out.println("생년월일(yyyy-mm-dd) > ");
		mbVO.setBirthdate(sc.nextLine());
		
		System.out.println("성별 > ");
		mbVO.setGender(sc.nextLine());
		
		System.out.println("주소 > ");
		mbVO.setAddress(sc.nextLine());
		
		System.out.println("연락처 > ");
		mbVO.setTel(sc.nextLine());
		
		System.out.println("관리자 유무 > ");
		mbVO.setAdmim(sc.nextLine());
		
		return mbVO;
	}

	private MbVO inputMbInfo() {
		MbVO mbVO = new MbVO();

		System.out.println("이름 > ");
		mbVO.setName(sc.nextLine());

		return mbVO;
	}

	private int inputMemNo() {
		int memNo = 0;
		try {
			System.out.println("회원번호를 입력하세요.");
			memNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("회원번호는 숫자로 구성되어 있습니다.");
		}
		return memNo;
	}

}
