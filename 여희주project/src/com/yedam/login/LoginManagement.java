package com.yedam.login;

import java.util.Scanner;

import com.yedam.course.COManagement;
import com.yedam.course.CourseDAOImpl;
import com.yedam.member.MbDAOImpl;
import com.yedam.member.MbManagement;

public class LoginManagement {
	// 필드
	protected Scanner sc = new Scanner(System.in);
	
	// 생성자 -> run
	public void run() {
		while (true) {
			boolean role = selectRole2();
			
			menuPrint(role);

			int menuNo = menuSelect();

			if (menuNo == 2) {
				// 회원등록
				new MbManagement();
			} else if (menuNo == 1 && role) {
				// 수강신청
				new COManagement();
			} else if (menuNo == 9) {
				// 프로그램 종료
				exit();
				break;
			} else {
				// 입력오류
				showInputError();
			}
		}
	}

	// 메소드
	protected void menuPrint(boolean role) { //관리자한테 수강신청 안보이게 role 걸기(권한을 부여하기 위해 Enum타입으로 관리)
		                                     // boolean - 가져올 객체가 있으면 true 없으면 false
		String menu = "";
		if(role) {
			menu += "1.수강신청 ";
		}
		menu += "2.회원등록 9.종료 ";			
		
		System.out.println("===============================================");
		System.out.println(menu);
		System.out.println("===============================================");
	}

	protected int menuSelect() { //메뉴선택
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("숫자로 입력해주시기 바랍니다.");
		}
		return menuNo;
	}

	protected void exit() {
		System.out.println("프로그램을 종료합니다.");
	}

	protected void showInputError() {
		System.out.println("메뉴에서 입력해주시기 바랍니다.");
	}

	protected boolean selectRole() {
		//현재 로그인한 사람 권한 확인 //관리자
		String memberRole = LoginControl.getLoginInfo().getAdmim();
		if (memberRole.equals("0")) {
			return true;
		} else {
			return false;
		}
	}

	protected boolean selectRole2() {
		//현재 로그인한 사람 권한 확인 //회원
		String memberRole = LoginControl.getLoginInfo().getAdmim();
		if (memberRole.equals("1")) {
			return true;
		} else {
			return false;
		}
	}
}
