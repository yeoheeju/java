package com.yedam.login;

import java.util.Scanner;

import com.yedam.member.MbDAO;
import com.yedam.member.MbDAOImpl;
import com.yedam.member.MbVO;

public class LoginControl {
	private Scanner sc = new Scanner(System.in);
	private static MbVO loginInfo = null;
	//static new연산자 안쓰고 이름 그대로 써도 가능(LoginControl)

	public static MbVO getLoginInfo() {
		return loginInfo;
	}
	
	private MbDAO mbDAO = MbDAOImpl.getInstance();
 //타입은 인터페이스로 선언 = Impl 클래스를 생성 new연산자 - 다형성
	
	public LoginControl() {
		while (true) {
			menuPrint();
			int menuNo = menuSelect();
			if (menuNo == 1) {
				// 로그인
				login();
			} else if (menuNo == 2) {
				// 종료
				exit();
				break;
			} else {
				showInputError();
			}
		}
	}

	private void menuPrint() {
		System.out.println("===============================================");
		System.out.println("1.로그인  2.종료");
		System.out.println("===============================================");
	}

	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("숫자로 입력해주세요.");
		}
		return menuNo;
	}

	private void exit() {
		System.out.println("프로그램을 종료합니다.");
	}

	private void showInputError() {
		System.out.println("메뉴를 확인해주시기 바랍니다.");
	}

	private void login() {
		// 아이디와 비밀번호 입력
		MbVO inputInfo = inputMember();
		// 로그인 시도
		loginInfo = mbDAO.loginInfo(inputInfo);
		

		// 실패할 경우 메소드 종료
		if (loginInfo == null)
			return;

		// 성공할 경우 프로그램을 실행
		new LoginManagement().run();
	}

	private MbVO inputMember() {
		MbVO info = new MbVO();
		System.out.print("회원번호 > ");
		info.setMemNo(Integer.parseInt(sc.nextLine()));
		System.out.print("비밀번호 > ");
		info.setPw(sc.nextLine());

		return info;
	}
}
