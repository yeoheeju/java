package com.yedam.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAO {
	// DateBase 연결정보

	private String jdbcDriver = "org.sqlite.JDBC";
	private String jdbcUrl = "jdbc:sqlite:/C:/Users/admin/Desktop/MemberProject/ProjectDataBase.db";

	// 각 메소드에서 공통적으로 사용되는 변수 -> 필드
	protected Connection conn; //db 서버와 연결
	protected Statement stmt; // 객체생성 후 sql문을 완성하여 db에서 바로 처리
	protected PreparedStatement pstmt; // '?'가 포함된 sql문을 생성하고 '?' 자리만 바꿔가며 처리
	protected ResultSet rs; //executeQuary()에서 실행된 select문의 결과값을 가지고 있는 객체

	// 연결
	// 1. JDBC DRIVER LOADING
	// 2. CONNECTION
	public void connect() {
		try {
			Class.forName(jdbcDriver);
			conn = DriverManager.getConnection(jdbcUrl);
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC DRIVER LOADING FAIL");

		} catch (SQLException e) {
			System.out.println("DATABASE CONNECTION FAIL");
			e.printStackTrace();

		}
	}

	// 연결해제
	// 6. 자원해체
	public void disconnect() {
		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			if (pstmt != null)
				pstmt.close();
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			System.out.println("정상적으로 자원이 해제 되지 않았습니다.");
		}
	}
}
